import React from 'react'
import { fetchtopicById } from '../../services/topic-service'
import { CommentList } from '../../components/comment.list';

function topicId({topic}){
console.log(topic)
    return(
        <>
        <h1>{topic.titre}</h1>

<CommentList list={topic.comment}/>
</>
    )
    }

export default topicId;

    export async function getServerSideProps(context) {
        return {
          props: {
              topic:await fetchtopicById(context.query.id),
          }, // sera transmis au composant de page en tant qu'accessoires
        }
      }
    