import { fetchtopic } from "../../services/topic-service";
import { Topiclist } from "../../components/topic-list"
import React, { useState } from 'react'
import axios from "axios"
import { useRouter } from "next/router";






 const Topic= ({topic})=> {
  const [posts, setPost] = useState(topic);
  const router = useRouter();
  const refreshData = () => {
    router.replace(router.asPath);
  }
  async function deleteTopic(id){
    await axios.delete(process.env.NEXT_PUBLIC_SERVER_URL + '/api/topic/' +id);
    setPost(
        posts.filter(item => item.id !== id)
    )
    refreshData()
    
}


    return (
        <div>
        <Topiclist  list={topic} onDelete={deleteTopic}/>
        </div>
    )

}
export default Topic

export async function getServerSideProps() {
    return {
      props: {
          topic:await fetchtopic(),
      }, 
    }
  }