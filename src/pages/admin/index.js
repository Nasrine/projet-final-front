import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useState } from 'react'
import ArticleForm from '../../components/ArticleForm'
import TopicForm from '../../components/TopicForm';
import api from '../../services/axios';


function Adminpage({article,href}) {
  const router = useRouter()

    const [posts, setPost] = useState(article);

    const addPost = async(post)=>{
        try {
            const add = await api.post("http://localhost:8000/api/article", post,
            )
            console.log(add+ " add");
           
        
            router.push("/article")
        } catch (error) {
            console.log (error);
            
        }

    }
const addTopic = async (post)=>{
    try {
       
        const add = await api.post("http://localhost:8000/api/topic", post,
        )
        
        router.push("/topic")
    } catch (error){
        console.log(error);
    }
        
    }

    const handleClick = (e) => {
        e.preventDefault()
        
    } 

    async function deleteProduct(id){
        await api.delete(process.env.NEXT_PUBLIC_API_URL + '/api/product/' +id);
        setPost(
            posts.filter(item => item.id !== id)
        )
    }
    

    return (
        <div className="formss" >
             <h1>Bienvenue Sur les formulaire D'ajout</h1>
          <div className="Titre">
           
            <div>
                <ArticleForm onSubmit={addPost}/>
            </div>
            <div>
                <TopicForm onSubmit={addTopic}/>
            </div>
            
        </div>
        </div>
      
        
    )
}

export default Adminpage
