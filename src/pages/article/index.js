import { ArticleList } from "../../components/articleslist"
import { fetchArticles } from "../../services/article-service"
import ArticleCard  from "../../components/article-card"
import CardArticle from "../../components/card-article"
import { useState } from "react"
import { useRouter } from "next/router"
import axios from "axios"


const articles = ({article}) => {
    const [posts, setPost] = useState(article);
    const router = useRouter();
    async function deleteArticle(id){
      await axios.delete(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' +id);
      setPost(
          posts.filter(item => item.id !== id)
      )
      refreshData()
      
  }
  const refreshData = () => {
    router.replace(router.asPath);
  }

    return (
        <div>

        
        
        <div>
            <ArticleList list={article} onDelete={deleteArticle} />
        </div>
        </div>
    )
}

export default articles


export async function getServerSideProps() {
    return {
      props: {
          article:await fetchArticles(),
      }, // sera transmis au composant de page en tant qu'accessoires
    }
  }
