import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"
import axios from 'axios';

export default NextAuth({
    jwt: {
      secret: process.env.JWT_SECRET
    },
    callbacks: {
        async jwt({ token, user }) {
          if (user) {
            return {
              accessToken: user.token,
              user: user.user,
            }
          }
    
          return token;
        },
        async session({ session, token }) {
          session.accessToken = token.accessToken;
          session.user = token.users
          return session;
        }
      },
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        email: { label: "email", type: "email", placeholder: "Your email..." },
        motdepasse: { label: "motdepasse", type: "password", placeholder: "Your Password..." }
      },
      async authorize(credentials, req) {
        try {
          const response = await axios.post("http://localhost:8000/api/user/login", credentials);
          const data = response.data;
          if (data.user && data.token) {
            return data;
          }
          return null;
        } catch (error) {
          console.log("error");
          return null;
        }
      }
    }),
    CredentialsProvider({
      name: "Sign-Up",
      type: "credentials",
      id: "signup",
      credentials: {
          nom: { label: "Nom", type: 'text' },
          email: { label: "email", type: "text", placeholder: "Exemple : test@gmail.com" },
          motdepasse: { label: "motdepasse", type: "password" },
  
      },
      authorize: async (credentials) => {
          try {
              const { data } = await axios.post("http://localhost:8000/api/user/register", {
                  nom: credentials?.nom,
                  email: credentials?.email,
                  motdepasse: credentials?.motdepasse,
      
              })
              console.log (data)
              if (data) {
                  return { status: 'success', data: data }

              } else {
                  return data
              }
          } catch (e) {

              throw new Error(e.response.data.error)

          }

      }
  }), 
  ],
})
