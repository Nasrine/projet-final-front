import '../styles/globals.css';
import ResponsiveAppBar from '../components/navbar'
import { makeStyles } from '@mui/styles';
import {SessionProvider} from 'next-auth/react'
import Footer from '../components/Footer';
import '../components/Footer.css'





function MyApp({ Component, pageProps:{session,...pageProps} }) {
  const classes = useStyles();
  return (
<div>
<SessionProvider session={session}>
      

     <ResponsiveAppBar/> 

<Component {...pageProps} />
<div className="center">

<Footer/>  
</div>

</SessionProvider>



</div>

//NAVBAR


//FOOTER

  )
}
const useStyles = makeStyles({
  root: {
    textAlign: 'center',

  },
  
});
export default MyApp
