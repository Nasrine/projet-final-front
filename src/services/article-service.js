import axios from "axios";




export async function fetchArticles(){ console.log(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article')
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article')
    return response.data
}

export async function fetchArticleById(id) {
    const response= await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/article/'+ id)
    return response.data
}

export async function postArticleComment(id, comment) {
    const session = await getSession()
    const user = session?.user
    console.log(id, comment)
    const response= await api.post(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' + id + '/comment', 
    {description: comment})

    return response.data
}

    
