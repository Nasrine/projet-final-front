import axios from "axios";




export async function fetchtopic(){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/topic')
    return response.data
}

export async function fetchtopicById(id) {
    const response= await axios.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/topic/'+ id)
    return response.data
}
