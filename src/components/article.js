import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteIcon from '@material-ui/icons/Delete';
import { useState } from 'react';
import axios from 'axios';
import { Grid } from '@mui/material'


const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));



export default function RecipeReviewCard({article,onDelete}) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  console.log(article)

  return (
    <div classname="grid">
      <Grid m={4} container spacing={6}>
        
      <Grid item  lg={11}>
    <Card  sx={{ maxWidth: 345 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
              N
          </Avatar>
        }
        action={
          <IconButton aria-label="delete">
              <DeleteIcon onClick={() => onDelete(article.id)} />
             </IconButton>
        }
        title={article.texte}
        subheader={article.date}
      />
      
      
      <CardMedia
        component="img"
        height="194"
        image={article.image}
        alt="card"
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {article.texte}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Recette:</Typography>
          <Typography paragraph>
          Commencer par mixer le sucre glace avec la poudre d'amande dans un mixeur. Passer au tamis (il faut que la poudre soit la plus fine possible, enlever les impuretés).
          </Typography>
          <Typography paragraph>
          Battre le blanc en neige et ajouter les 10 g de sucre, et le colorant, peu à peu en mixant jusqu'à ce que les blancs soient bien figés.
          </Typography>
          <Typography paragraph>
          Ajouter le sucre glace + les amandes en poudre au blanc en neige et mélanger délicatement avec une spatule afin de "casser" un peu les blancs.
          Mettre la pâte à macaron dans une poche à douille et faire des petits tas sur une plaque recouverte de papier sulfurisé puis laisser reposer les macarons pendant 15 min.

          </Typography>
          <Typography>
          Préchauffer le four à 140°C (thermostat 4-5) avec une plaque à l'intérieur pour qu'elle chauffe. Enfourner la plaque de macarons sur la plaque déjà chaude pour 10 min de cuisson chaleur tournante et porte entrouverte.
          Une fois cuits, sortir la plaque du four, et verser un peu d'eau sous la feuille de papier sulfurisé. Cela va dégager de la vapeur qui permet de bien décoller les macarons.
          Il ne reste plus qu'à les fourrer avec la ganache de votre choix ! Par exemple, chocolat blanc ou crème au beurre.


          </Typography>
        </CardContent>
      </Collapse>
    </Card>
    </Grid>
    </Grid>
    </div>
  );
}
