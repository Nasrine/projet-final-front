import { Grid } from '@mui/material'
import {CommentCard} from "./comment-card"



export const CommentList = ({ list, onDelete }) => {
console.log(list)
    return (
        <Grid container spacing={1} >

            
            {list.map(item =>
                    <CommentCard key={item.id} commentaire={item} onDelete={onDelete} />)}


        

</Grid>      
    )
}
