import { Grid } from '@mui/material'
import Article from './article'



export const ArticleList = ({ list, onDelete }) => {

    return (
        // <Grid container spacing={1} >

        <div>
            <Grid container={2}>
            <Grid container spacing={{ xs: 1, md: 1 }} columns={{ xs: 4, sm: 3, md: 16}}>

                {list.map(item =>
                    <Grid item xs={12} sm={8} md={5} key={item.id}>

                        <Article article={item} onDelete={onDelete} />  
                    </Grid>)}

                      </Grid>
                      </Grid>
                      </div>
                )
                }
 

                