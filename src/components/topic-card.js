import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Link from 'next/link';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ShareIcon from '@mui/icons-material/Share';
import { Grid } from '@mui/material'



const useStyles = makeStyles({
    root: {
        minWidth: 275,
      },
      bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
      },
      title: {
        fontSize: 14,
      },
      pos: {
        marginBottom: 12,
      },
    });
 export const Topiccard=({topic,onDelete})=> {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;

    return (
      <div className="grid">
 <Grid m={4} container spacing={6}>
         <Grid item  lg={11}></Grid>
         <Card  sx={{ maxWidth: 345 }}>


        
        <div>
            <Card className={classes.root}>
      <CardContent>
      
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          {topic.date} <IconButton aria-label="delete">
              <DeleteIcon onClick={() => onDelete(topic.id)} />
             </IconButton>
        </Typography>
        <Typography variant="h5" component="h2">
        {topic.titre}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {topic.descripation}
        </Typography>
        <Typography variant="body2" component="p">
        {topic.utilisateur}
          <br />
          {'"a benevolent smile"'}
        </Typography>
      </CardContent>
      
      <Link href={'/topic/' + topic.id}>
      Commentaire
                </Link>
      
    </Card>

        </div>
        </Card>

        </Grid>
      </div>
     
    )

}
  
