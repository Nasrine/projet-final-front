

// function CommentsAdd({ istopic = false, id }:  {
//     type FormData = {
//         comment: string;
//     };
    
    function CommentsAdd({ istopic = false, id }) {
    
        const { data: session } = useSession()
        const router = useRouter();
        const { register, handleSubmit } = useForm();
    
        const refreshData = () => {
            router.replace(router.asPath);
        }
    
        async function onSubmit(data) {
            
                await postTopicComment(id, data.comment)
           
            refreshData()
        }
    
    
        if (session) {
            return (
                <div>
    
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="row mx-5 p-2">
    
                        <textarea {...register("comment", {})} />
    
    
                        </div>
    
                        <div className="row mx-5 pb-5 p-2">
                        
                            <input type="submit"  className="btn background-button-color"/>
    
                        </div>
                    </form>
    
    
                </div>
    
    
            )
        }
    
    
        return (
            <div className="first-text-neutral d-flex justify-content-center">
            <a href="/api/auth/signin">Sign in to interact with us!</a>
    
            </div>
        )
    }
    
    export default CommentsAdd