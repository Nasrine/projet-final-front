import { Topiccard } from "./topic-card"
import React from 'react';
import { Grid } from '@mui/material'


export const  Topiclist=({list,onDelete })=> {


    return (
        
        <div>
            <Grid container={2}>
            <Grid container spacing={{ xs: 1, md: 1 }} columns={{ xs: 3, sm: 3, md: 12}}>
            
            {list.map(item =>
            <Topiccard key={item.id } topic={item} onDelete={onDelete}/>)}
            </Grid>
            </Grid>
             
        </div>
    )

}
  