import { Grid } from '@mui/material'
import Card from '@mui/material/Card';
import { useSession } from 'next-auth/react'
import React, { useState } from 'react'
import { useRouter } from 'next/router'


function TopicForm({onSubmit,href}) {
    const router = useRouter()

    const { data: session } = useSession()
    const user = session?.user

    const initial = {
        titre: '',
        descripation: '',
        utilisateur: '',
        date: '',
        id: ''
    }

    const [form, setForm] = useState(initial)

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }
    const handleSubmit = (event) => {
        console.log("coucou")
        event.preventDefault();
        onSubmit(form);
    }
  
    

    return (
        <>
        {session  &&
            <div className="">
                 <Grid item  lg={11} ml={11}>
    <Card  sx={{ maxWidth: 345 }}>
                <h1 className="formtopic">Formulaire  Topic</h1>
                <div className="topic">
                    <form className="mt-5" onSubmit={handleSubmit}>
                    <div className="border border-gray-500 p-2">
                            <input placeholder="titre" type="text" onChange={handleChange} name="titre" value={form.titre} required />
                        </div>
                        <div className="border border-gray-500 p-2">
                            <input placeholder="descripation" type="text" onChange={handleChange} name="descripation" value={form.descripation} required />
                        </div>
                        <div className="border border-gray-500 p-2">
                            <input placeholder="utilisateur" type="text" onChange={handleChange} name="utilisateur" value={form.utilisateur} required />
                        </div>
                        <div className="border border-gray-500 p-2">
                            <input placeholder="date" type="text" onChange={handleChange} name="date" value={form.date} required />
                        </div>
                       
                            <div className="border bg-[#ba9f87] text-center border-black p-2">
                                <button>Ajouter</button>
                        </div>
                        
                    </form>
                </div>
                </Card>
    </Grid>
            </div>
       
        }

    </>
    
    )
}

export default TopicForm
