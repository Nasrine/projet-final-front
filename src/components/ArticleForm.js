import { useSession } from 'next-auth/react'
import React, { useState } from 'react'
import { useRouter } from 'next/router'



function ArticleForm({onSubmit,href}) {
    const router = useRouter()

    const { data: session } = useSession()
    const user = session?.user

    const initial = {
        image: '',
        texte: '',
        date: '',
        auteur: '',
        id: ''
    }

    const [form, setForm] = useState(initial)
    console.log(form);
    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }
    const handleSubmit = (event) => {
        event.preventDefault();
        onSubmit(form);
    }
   
    return (
        <>
        {session  &&
            <div className="flex flex-col lg:flex-row justify-around items-center border-b  border-black Formulaire">
                <h1 className="">Formulaire Article</h1>
                <div className="formArt p-2 mb-5">
                    <form className="mt-5" onSubmit={handleSubmit}>
                    <div className="border border-gray-500 p-2">
                            <input placeholder="image" type="text" onChange={handleChange} name="image" value={form.image} required />
                        </div>
                        <div className="border border-gray-500 p-2">
                            <input placeholder="texte" type="text" onChange={handleChange} name="texte" value={form.texte} required />
                        </div>
                        <div className="border border-gray-500 p-2">
                            <input placeholder="date" type="text" onChange={handleChange} name="date" value={form.date} required />
                        </div>
                        <div className="border border-gray-500 p-2">
                            <input placeholder="auteur" type="text" onChange={handleChange} name="auteur" value={form.auteur} required />
                        </div>
                       
                            <div className="border bg-[#ba9f87] text-center border-black p-2">
                                <button  >Ajouter</button>
                        </div>

                    </form>
                </div>
            </div>

        }

    </>

    )
}


export default ArticleForm
