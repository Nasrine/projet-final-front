import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Link from 'next/link';

export default function CardArticle({article}) {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardMedia
        component="img"
        height="140"
        image="image/cupcake.jpeg"
        alt="green iguana"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Forum
        </Typography>
        <Typography variant="body2" color="text.secondary"> 
       
        </Typography>
      </CardContent>
      
      <CardActions>
      <Link href="/topic"  >

       <Button size="small"className='form'>Découvrir le forum</Button>
       </Link>
      </CardActions>
    </Card>
    
  );
}